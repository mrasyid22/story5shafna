from django.shortcuts import render, get_object_or_404, redirect
from .models import MataKuliah
from .forms import *
from django.contrib import messages
from django.contrib.messages import constants

def index(request):
    semua_matkul = MataKuliah.objects.all()
    return render(request, 'matakuliah.html', {'all_matkul': semua_matkul})

def detail(request, matkul_id):
    matkul = get_object_or_404(MataKuliah, pk=matkul_id)
    return render(request, 'detail.html', {'matkul': matkul})

def add(request):
    if request.method == "POST":
        form = MataKuliahForm(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, (f"Course {request.POST['nama_matkul']} successfully added!"))
            redirect('/')
        else:
            messages.warning(request, ("Data invalid."))
            redirect('/')
    return render(request, 'add.html')

def delete(request, id):
    matkul = MataKuliah.objects.get(id=id)
    matkul.delete()
    messages.success(request, (f"Course {matkul} deleted"))
    return redirect('storylima:index')
