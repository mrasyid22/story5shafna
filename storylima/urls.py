from . import views
from django.contrib import admin
from django.urls import path,include
from django.conf import settings
from django.conf.urls.static import static
# from .views import matakuliah

app_name = 'storylima'

urlpatterns = [
    path('',views.index,name='index'),
    path('<int:matkul_id>/', views.detail, name='detail'),
    path('add/', views.add, name='add'),
    path('delete/<int:id>', views.delete, name='delete'),
]
