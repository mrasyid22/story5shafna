from django import forms
from .models import *

class MataKuliahForm(forms.ModelForm):

    class Meta:
        model = MataKuliah
        fields = '__all__'
